Projekt von Aleksej Kelm und Max Steube-Sehr


Wir werden das Spiel 4 Gewinnt mit hilfe der Platform Bord of Symbols erstellen.
Es ist ein spiel für den Zeitvertreib.

Funktionalität
die Funktionalität von 4 gewinnt werden wir so darstellen, dass es für zwei Spieler
programmiert wird. Jeder spieler erhält eine eigene Farbe, dei von uns vorgegeben
wird. Das Spielfeld ist begrenzt. Wir werden die Größe auf die bekannte größe von 
6x5 feldern festlegen. Nun kann man abwächselnd reihen anklicken und ein Punkt 
wird die Spalte runterfallen. Hierbei wird berücksichtigt, dass die farben sich 
nicht "überschreiben" sprich, falls schon ein Punkt in einer Spalte ist bleibt der 
Nächste punkt darüber stehen. Spielende wird sein, wenn man 4 punkte nebeneinander
oder diagonal aneinander gereiht hat.

Die Größte Herrausvorderung
Die vermutlich größte Herrausvorderung wird sein, ein Ende zu definieren und
nicht nur wenn die punkte waagerecht oder senkrecht, sondern auch diagonal eine
4er Kette bilden. 

Lösungsansatz

Wir stellen uns vor eine art koordinatensystem zur programmierung zu ziehen, ähnlich
wie damals bei den Pyramiden aufgaben in BoS, Lediglich mit der Einschränkungen, 
dass bei einer 4er Kette der selben farbe das Spiel beendet wird.